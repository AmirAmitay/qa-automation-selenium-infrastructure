﻿using OpenQA.Selenium;
using System.Reflection;

namespace Selenium.PageHelpers
{
    public abstract class PageBase
    {
        private static Dictionary<By, string> _pagesElementsNames = new Dictionary<By, string>();

        public PageBase() { }

        public PageBase(Type pageType)
        {
            AddBysAndAttributesNameToDictionary(pageType);
        }

        protected static void AddBysAndAttributesNameToDictionary(Type pageType)
        {
            try
            {
                var properties = pageType.GetProperties().ToList();
                var methods = pageType.GetMethods().ToList();
                var obj = Activator.CreateInstance(pageType);

                properties.ForEach(p =>
                {
                    methods.ForEach(m =>
                    {
                        if (m.GetParameters().Length == 0 && m.ReturnParameter.ParameterType == typeof(By))
                        {
                            if (m.Name.Equals(p.GetGetMethod().Name))
                            {
                                try
                                {
                                    var by = (By)m.Invoke(obj, null);
                                    var attribute = (ElementNameAttribute)p.GetCustomAttribute(typeof(ElementNameAttribute));
                                    var name = attribute.Name;
                                    if (!_pagesElementsNames.ContainsKey(by))
                                    {
                                        _pagesElementsNames.Add(by, name);
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }
                    });
                });
            }
            catch
            {

            }
        }

        protected static void ManualAddToElementsDictionary(By key, string value)
        {
            if (!_pagesElementsNames.ContainsKey(key))
            {
                _pagesElementsNames.Add(key, value);
            }
        }

        public static string GetElementName(By element)
        {
            if (_pagesElementsNames.ContainsKey(element))
            {
                return _pagesElementsNames[element];
            }
            else
            {
                return string.Empty;
            }
        }
    }
}