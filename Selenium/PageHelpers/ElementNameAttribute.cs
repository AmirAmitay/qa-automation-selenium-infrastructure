﻿namespace Selenium.PageHelpers
{
    public class ElementNameAttribute : Attribute
    {
        public string Name;

        public ElementNameAttribute(string name)
        {
            this.Name = name;
        }
    }
}