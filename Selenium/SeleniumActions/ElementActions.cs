﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Selenium.WebDriver;
using Selenium.Enums;
using Selenium.Enums.EnumHelpers;
using static Logger.Logger;
using Selenium.PageHelpers;

namespace Selenium.SeleniumActions
{
    public abstract class ElementsActions : Alerts
    {
        public ElementsActions(Driver driver) : base(driver) { }

        protected void Click(By by, int timeout = 7, bool doubleClick = false)
        {
            try
            {
                var click = doubleClick ? "double click" : "click";
                Log.Info($"Try to {click} on: [{PageBase.GetElementName(by)}]");
                WaitForElementToBeExists(by, timeout);
                WaitForElementToBeClickable(by, timeout);
                IWebElement element = _driver.GetElement(by);

                HighlightElementAndRemoveHighlight(element);

                GetElementCheckIfDoubleClickAndClick(by, doubleClick);
            }
            catch (StaleElementReferenceException)
            {
                GetElementCheckIfDoubleClickAndClick(by, doubleClick);
            }
            catch (ElementClickInterceptedException)
            {
                GetElementCheckIfDoubleClickAndClick(by, doubleClick);
            }
            catch (Exception ex)
            {
                Log.Error($"Click on: [{PageBase.GetElementName(by)}] failed");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        private void GetElementCheckIfDoubleClickAndClick(By by, bool doubleClick)
        {
            IWebElement element = _driver.GetElement(by);

            if (doubleClick)
            {
                DoubleClick(element);
            }
            else
            {
                element.Click();
            }
        }

        protected void Click(IWebElement element, int timeout = 7, bool doubleClick = false)
        {
            try
            {
                var click = doubleClick ? "double click" : "click";
                Log.Info($"Try to {click} on Element");

                WaitForElementToBeClickable(element, timeout);
                HighlightElementAndRemoveHighlightAndClickOnElement(element, doubleClick);
            }
            catch (Exception ex)
            {
                Log.Error($"Click on: Element failed");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void RightClick(By by, int timeout = 7)
        {
            try
            {
                Log.Info($"Try to Right Click on: [{PageBase.GetElementName(by)}]");
                WaitForElementToBeClickable(by, timeout);
                var element = _driver.GetElement(by);
                HighlightElementAndRemoveHighlightAndRightClickOnElement(element, timeout);
            }
            catch (Exception ex)
            {
                Log.Error($"Click on: Element failed");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void RightClick(IWebElement element, int timeout = 7)
        {
            try
            {
                Log.Info($"Try to Right Click on Element");
                WaitForElementToBeClickable(element, timeout);
                HighlightElementAndRemoveHighlightAndRightClickOnElement(element, timeout);
            }
            catch (Exception ex)
            {
                Log.Error($"Click on: Element failed");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void ChangeElementAttributeValue(By by, HtmlAttributesEnum attribute, string value)
        {
            Log.Info($"Try to change {PageBase.GetElementName(by)} attribute [{attribute.GetDescription()} value to:{value}]");

            try
            {
                _driver.GetJavaScriptExecutor().ExecuteScript($"arguments[0].setAttribute('{attribute.GetDescription()}', '{value}')", _driver.GetElement(by));
                Log.Info($"Attribute change SUCCESS");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected IWebElement SwitchToActiveElement()
        {
            try
            {
                Log.Info("Try to switch to active element");
                return _driver.GetDriver().SwitchTo().ActiveElement();
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }

        }

        protected void ClickWithJavaScript(By by, int timeout = 5)
        {
            Log.Info($"Try to click on: [{PageBase.GetElementName(by)}]");

            try
            {
                var element = _driver.GetElement(by);
                WaitForElementToBeClickable(by, timeout);
                _driver.GetJavaScriptExecutor().ExecuteScript("arguments[0].click();", element);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void ClickWithJavaScript(IWebElement element, int timeout = 5)
        {
            Log.Info($"Try to click on: Element]");

            try
            {
                WaitForElementToBeClickable(element, timeout);
                _driver.GetJavaScriptExecutor().ExecuteScript("arguments[0].click();", element);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void SetTextWithJavaScript(By by, string value)
        {
            Log.Info($"Try to set text: {value}, to: [{PageBase.GetElementName(by)}]");

            try
            {
                var element = _driver.GetElement(by);
                _driver.GetJavaScriptExecutor().ExecuteScript("arguments[0].value = arguments[1]; ", element, value);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void UploadFile(By by, string path, int timeout = 5)
        {
            try
            {
                Log.Info($"Try To Upload File With: {PageBase.GetElementName(by)}");
                Log.Info($"The Path: {path}");
                IsElementExists(by, timeout);
                IWebElement element = _driver.GetElement(by);
                element.SendKeys(path);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void SetText(By by, object value, bool clear = true)
        {
            try
            {
                Log.Info($"Try to set text: {value}, to: [{PageBase.GetElementName(by)}]");

                WaitForElementToBeClickable(by);
                IWebElement element = _driver.GetElement(by);
                var oldStyle = ElementHighlight(element, true);

                if (clear)
                {
                    element.Clear();
                }

                element.SendKeys(value.ToString());
                RemoveHighlight(element, oldStyle);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void ClearText(By by)
        {
            try
            {
                Log.Info($"Try to clear text from: [{PageBase.GetElementName(by)}]");
                WaitForElementToBeClickable(by);
                IWebElement element = _driver.GetElement(by);
                var oldStyle = ElementHighlight(element, true);
                element.Clear();
                RemoveHighlight(element, oldStyle);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void SelectItemFromComboBoxByText(By by, string value = "")
        {
            try
            {
                Log.Info($"Try to select item from: [{PageBase.GetElementName(by)}], by text: [{value}]");
                SelectElement selectElement = GetComboBox(by);
                selectElement.SelectByText(value);
                Log.Info($"{value} From {PageBase.GetElementName(by)} Is Selected");
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void SelectItemFromComboBoxByValue(By by, string value = "")
        {
            try
            {
                Log.Info($"Try to select item from: [{PageBase.GetElementName(by)}], by value: [{value}]");

                SelectElement selectElement = GetComboBox(by);
                selectElement.SelectByValue(value);
                Log.Info($"{value} From {PageBase.GetElementName(by)} Is Selected");
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        private SelectElement GetComboBox(By by)
        {
            try
            {
                Log.Info($"Try to Get: [{PageBase.GetElementName(by)}]");
                WaitForElementToBeClickable(by);
                var comboBox = _driver.GetElement(by);
                HighlightElementAndRemoveHighlight(comboBox);
                return new SelectElement(comboBox);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void SetTextToActiveElement(string value, bool clear = true)
        {
            try
            {
                Log.Info($"Try to set text:[{value}], to the active element");
                var element = SwitchToActiveElement();

                if (clear)
                {
                    element.Clear();
                }

                element.SendKeys(value);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void SendArrowDownToActiveElement()
        {
            try
            {
                Log.Info($"Try to send [ArrowDown] to the active element");
                var element = SwitchToActiveElement();
                WaitForElementToBeClickable(element);
                element.SendKeys(Keys.ArrowDown);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void SendEnterToActiveElement()
        {
            try
            {
                Log.Info($"Try to send [ENTER] to the active element");
                var element = SwitchToActiveElement();
                WaitForElementToBeClickable(element);
                element.SendKeys(Keys.Enter);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void SelectItemInsideElement(By by, string tagName, string secondTagName = "", string value = "", int timeout = 10)
        {
            try
            {
                var element = WaitForItemAppearInElementByTagName(by, tagName, secondTagName, value, timeout);
                HighlightElementAndRemoveHighlight(element);
                element.Click();
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void SelectSpanFromLi(By ul, string value)
        {
            Log.Info($"Try to find {value} in ul: {PageBase.GetElementName(ul)}, [<span>]");
            SelectItemInsideElement(ul, "li", "span", value);
        }

        protected void Select_A_FromLi(By ul, string value)
        {
            Log.Info($"Try to find {value} in ul: {PageBase.GetElementName(ul)}, [<a>]");
            SelectItemInsideElement(ul, "a", value);
        }

        protected List<IWebElement> GetElementsList(By by)
        {
            var list = _driver.GetElementsList(by);
            Log.Info($"Found: [{list.Count}] elements, in {PageBase.GetElementName(by)}");
            return list;
        }

        protected void SelectItemFromComboBoxByIndex(By by, int index)
        {
            try
            {
                WaitForElementToBeClickable(by);
                Log.Info($"Try To Select Item From: {PageBase.GetElementName(by)}, By Index: [{index}]");
                var comboBox = _driver.GetElement(by);
                SelectElement selectElement = new SelectElement(comboBox);
                selectElement.SelectByIndex(index);
                Log.Info($"{index} From {PageBase.GetElementName(by)} Is Selected");
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected int GetOptionsCountFromComboBox(By by)
        {
            try
            {
                Log.Info($"Try To Get Options Count From: {PageBase.GetElementName(by)}");
                var optionsCount = GetComboBox(by).Options.ToList().Count;
                Log.Info($"Options Count = [{optionsCount}]");
                return optionsCount;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected string GetElementText(By by, bool wait = false)
        {
            try
            {
                Log.Info($"Try to get text of: {PageBase.GetElementName(by)}");
                var text = _driver.GetElement(by).Text;

                if (wait)
                {
                    WaitForTextAppearInElement(by);
                }

                Log.Info($"The text is: '{text}'");

                return text;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return string.Empty;
            }
        }

        protected string GetAttributeValue(By by, HtmlAttributesEnum attribute)
        {
            try
            {
                var attributeValue = _driver.GetElement(by).GetAttribute(attribute.GetDescription());
                Log.Info($"Attribute[{attribute}] value of: [{PageBase.GetElementName(by)}], is: [{attributeValue}]");
                return attributeValue;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return string.Empty;
            }
        }

        protected void ClickOnElementFromTableByTagName(By table, int buttonRow, int buttonColumn, string tagName)
        {
            try
            {
                Log.Info($"Try to click on <{tagName}> from table: [{PageBase.GetElementName(table)}], Row Index = [{buttonRow}], Column Index = [{buttonColumn}]");
                var element = _driver.GetElement(table).FindElements(By.XPath("tr")).ToList()[buttonRow].FindElements(By.TagName("td")).ToList()[buttonColumn].FindElement(By.TagName(tagName));
                HighlightElementAndRemoveHighlight(element);
                element.Click();
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected string GetValueOfCellFromTable(By table, int row, int column, int tableIndex = 0)
        {
            try
            {
                Log.Info($"Try to get value from table: [{PageBase.GetElementName(table)}], Row Index = [{row}], Column Index = [{column}]");
                var value = _driver.GetElementsList(table)[tableIndex].FindElements(By.XPath("tr")).ToList()[row].FindElements(By.TagName("td")).ToList()[column].Text;
                Log.Info($"Cell Value Is: {value}");
                return value;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return string.Empty;
            }
        }

        protected void ClickOnTableRow(By table, int row, int column, bool doubleClick = false)
        {
            try
            {
                var click = doubleClick ? "double click" : "click";
                Log.Info($"Try to {click} on row from table: [{PageBase.GetElementName(table)}], Row Index = [{row}]");
                var element = _driver.GetElement(table).FindElements(By.XPath("tr")).ToList()[row].FindElements(By.TagName("td")).ToList()[column];
                HighlightElementAndRemoveHighlight(element);

                if (doubleClick)
                {
                    DoubleClick(element);
                }
                else
                {
                    element.Click();
                }
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void SendKeysToAutoCompleteInput(By by, string value, bool clear = true)
        {
            try
            {
                Log.Info($"Try to send: [{value}] + [Enter] To Auto Complete Element");
                Actions actions = new Actions(_driver.GetDriver());
                var element = _driver.GetElement(by);

                if (clear)
                {
                    element.Clear();
                }

                element.SendKeys(value);
                actions.SendKeys(element, Keys.Enter).Perform();
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        private void DoubleClick(IWebElement element)
        {
            try
            {
                Actions action = new Actions(_driver.GetDriver());
                action.DoubleClick(element).Perform();
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected int GetTableRowsCount(By table)
        {
            try
            {
                var rowCount = _driver.GetElement(table).FindElements(By.XPath("tr")).Count;
                Log.Info($"Table: {PageBase.GetElementName(table)}, Rows Count = [{rowCount}]");
                return rowCount;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void MoveToElement(By element, int timeout = 10)
        {
            try
            {
                Log.Info($"Try to move to element: [{PageBase.GetElementName(element)}]");
                Actions actions = new Actions(_driver.GetDriver());
                IsElementIsVisible(element, timeout);
                actions.MoveToElement(_driver.GetDriver().FindElement(element)).Click().Perform();
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void ScrollToElement(By by)
        {
            try
            {
                Log.Info($"Try to scroll to element: [{PageBase.GetElementName(by)}]");
                var element = _driver.GetDriver().FindElement(by);
                ScrollToElement(element);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void ScrollToElement(IWebElement element)
        {
            _driver.GetJavaScriptExecutor().ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        private void HighlightElementAndRemoveHighlightAndClickOnElement(IWebElement element, bool doubleClick)
        {
            HighlightElementAndRemoveHighlight(element);

            if (doubleClick)
            {
                DoubleClick(element);
            }
            else
            {
                element.Click();
            }
        }

        private void HighlightElementAndRemoveHighlightAndRightClickOnElement(IWebElement element, int timeout)
        {
            HighlightElementAndRemoveHighlight(element);
            Actions actions = new Actions(_driver.GetDriver());
            actions.ContextClick(element).Perform();
        }
    }
}