﻿using OpenQA.Selenium;
using Selenium.WebDriver;
using SeleniumExtras.WaitHelpers;
using Selenium.PageHelpers;
using System.Diagnostics;
using Selenium.Exceptions;
using static Logger.Logger;

namespace Selenium.SeleniumActions
{
    public abstract class DriverWaitActions : HighlightElementsActions
    {
        public DriverWaitActions(Driver driver) : base(driver) { }

        protected void SwitchFrame(By frameName)
        {
            try
            {
                var wait = _driver.GetWebDriverWait();
                Log.Info($"Try to switch to frame: [{PageBase.GetElementName(frameName)}]");
                wait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(frameName));
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected void ChangeFrameForDisappearedElement(By frame, int timeout = 10)
        {
            try
            {
                Log.Info($"Try to get element list of: [iframes]");

                var frames = _driver.GetElementsList(By.TagName("iframe"), false);
                var frameName = PageBase.GetElementName(frame);

                if (frames.Any(f => f.GetAttribute("name").Equals(frameName)))
                {
                    frames.ForEach(f => Log.Info(f.GetAttribute("name")));
                    var wait = _driver.GetWebDriverWait(timeout);
                    Log.Info($"Try to switch to frame: [{PageBase.GetElementName(frame)}]");
                    wait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(frame));
                }
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected bool WaitForElementToBeExists(By by, int timeout = 5)
        {
            try
            {
                Log.Info($"Wait For Element: {PageBase.GetElementName(by)}, To Be Exists");
                _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementExists(by));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                throw;
            }

            return true;
        }

        protected void WaitForAttributeValueToBe<T>(By by, string attribute, T expectedValue, int timeout = 10)
        {
            Log.Info($"Wait For Element: [{PageBase.GetElementName(by)}], Attribute <{attribute}> To Be: {expectedValue}");
            var wait = _driver.GetWebDriverWait(timeout);
            var attributeValue = string.Empty;

            try
            {
                wait.Until((d) =>
                {
                    try
                    {
                        var element = _driver.GetElement(by, false);
                        attributeValue = element.GetAttribute(attribute);

                        if (string.IsNullOrEmpty(attributeValue))
                        {
                            return false;
                        }

                        var convertedAttributeValue = (T)Convert.ChangeType(attributeValue, typeof(T));

                        return EqualityComparer<T>.Default.Equals(convertedAttributeValue, expectedValue);
                    }
                    catch
                    {
                        return false;
                    }
                });
            }
            catch (WebDriverTimeoutException)
            {
                var ex = new AttributeNotUpdatedException($"Attrubte:<{attribute}>, Not Updated.\n Expected Value Is [{expectedValue}], Actual Value Is: {attributeValue}");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw ex;
            }
        }

        protected bool WaitForElementToBeClickable(By by, int timeout = 5)
        {
            try
            {
                Log.Info($"Wait For Element: [{PageBase.GetElementName(by)}], To Be Clickable");
                _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementToBeClickable(by));
            }
            catch (WebDriverArgumentException)
            {
                _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementToBeClickable(by));
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }

            return true;
        }

        protected bool WaitForElementToBeClickable(IWebElement element, int timeout = 5)
        {
            try
            {
                Log.Info($"Wait For Element To Be Clickable");
                _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementToBeClickable(element));
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }

            return true;
        }

        protected bool WaitForTextAppearInElement(By by, int timeout = 10)
        {
            try
            {
                Log.Info($"Wait For Element: {PageBase.GetElementName(by)}, Text To Appear");
                var element = _driver.GetElement(by);
                return _driver.GetWebDriverWait(timeout).Until(d => !d.FindElement(by).Text.Equals(string.Empty));
            }
            catch (WebDriverTimeoutException)
            {
                var ex = new ElementHaveNoTextException();
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void WaitForTextToPresentInElement(By by, string expectedText, int timeout = 10)
        {
            var actualText = string.Empty;
            try
            {
                _driver.GetWebDriverWait(timeout).Until(d =>
                {
                    try
                    {
                        actualText = string.Empty;
                        var element = _driver.GetDriver().FindElement(by);
                        actualText = element.Text.Trim();
                        return actualText.Equals(expectedText);
                    }
                    catch
                    {
                        return false;
                    }
                });
            }
            catch (WebDriverTimeoutException)
            {
                var ex = new TextNotPresentInElementException($"The Text: {expectedText}, Not Present In The Element.\n Actual Text Is: {actualText}");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw ex;
            }
        }

        protected void WaitForCursorStopLoading()
        {
            var timeout = 3;
            var isLoading = _driver.GetWebDriverWait(timeout).Until(d => d.FindElement(By.TagName("body")).GetCssValue("cursor") == "wait");

            if (isLoading)
            {
                _driver.GetWebDriverWait(timeout).Until(d => d.FindElement(By.TagName("body")).GetCssValue("cursor") == "auto");
            }
        }

        protected void WaitForTableRowCountToBeExpectedCount(By table, int expectedRowsCount, int timeout = 10)
        {
            Log.Info($"Expected Rows Count For: [{PageBase.GetElementName(table)}], Is: [{expectedRowsCount}]");
            var wait = _driver.GetWebDriverWait(timeout);
            var actualRowsCount = 0;
            try
            {
                Log.Info($"Try to get element: [{PageBase.GetElementName(table)}]");

                wait.Until((d) =>
                {
                    try
                    {
                        actualRowsCount = 0;
                        var rows = _driver.GetElement(table, false).FindElements(By.XPath("tr")).ToList();

                        Log.Info($"Rows Expected Count = [{expectedRowsCount}], Actual Rows Count = [{actualRowsCount}]");

                        return actualRowsCount == expectedRowsCount;
                    }
                    catch
                    {
                        return false;
                    }
                });
            }
            catch (WebDriverTimeoutException)
            {
                var ex = new TableRowsCountDiffrentFromTheExpectedCount($"The Expected Rows Count Is: [{expectedRowsCount}], The Actual Rows Count Is: {actualRowsCount}");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw ex;
            }
        }

        //TODO: Remove The SW in Education And check it
        protected IWebElement WaitForItemAppearInElementByTagName(By by, string tagName, string secondTagName = "", string value = "", int timeout = 10)
        {
            return _driver.GetWebDriverWait(timeout).Until((d) =>
            {
                IWebElement element = null;
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var secondTagDescription = "";

                if (!string.IsNullOrEmpty(secondTagName))
                {
                    secondTagDescription = $", Seocnd Tag Name = [<{secondTagName}>]";
                }

                Log.Info($"Try to find [{value}] in: [{PageBase.GetElementName(by)}],Tag Name = [<{tagName}>]{secondTagDescription}");

                while (element == null && stopWatch.Elapsed.TotalSeconds < timeout)
                {
                    List<IWebElement> list = null;

                    try
                    {
                        list = GetElementsInsideElementByTagName(by, tagName);
                        if (string.IsNullOrEmpty(secondTagName))
                        {
                            element = list.Select(i => GetElementIfTextEquals(i, value)).First();
                        }
                        else
                        {
                            var secondList = list.SelectMany(i => i.FindElements(By.TagName(secondTagName))).ToList();
                            element = secondList.Find(i => GetElementIfTextEquals(i, value)!=null);
                        }
                    }
                    catch
                    {
                        list = GetElementsInsideElementByTagName(by, tagName);
                    }
                }

                if (element == null)
                {
                    Log.Info($"No Elements Found!");
                }

                return element;
            });
        }

        private List<IWebElement> GetElementsInsideElementByTagName(By element, string tagName)
        {
            var list = _driver.GetElement(element).FindElements(By.TagName(tagName)).ToList();
            Log.Info($"Found: [{list.Count}] elements, in {PageBase.GetElementName(element)}, Tag Name = [{tagName}]");
            return list;
        }

        private IWebElement GetElementIfTextEquals(IWebElement element, string value)
        {
            if (element.Text.Equals(value) || element.GetAttribute("title").Equals(value))
            {
                Log.Info($"Element with text = [{value}], Found");
                WaitForElementToBeClickable(element);
                return element;
            }
            return null;
        }
    }
}