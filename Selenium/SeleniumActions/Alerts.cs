﻿using OpenQA.Selenium;
using Selenium.WebDriver;
using static Logger.Logger;

namespace Selenium.SeleniumActions
{
    public abstract class Alerts : BrowserActions
    {
        public Alerts(Driver driver) : base(driver) { }

        public static bool AlertFlag = false;

        protected string GetAlertText(int timeout = 2)
        {
            try
            {
                Log.Info("Try to get alert text....");
                var alertText = SwitchToAlert(timeout).Text;
                Log.Info($"Alert text: {alertText}");

                return SwitchToAlert(timeout).Text;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void ClickOnAcceptAlert(int timeout = 2)
        {
            try
            {
                Log.Info("Try to click on alert accept button");
                SwitchToAlert(timeout).Accept();
                AlertFlag = false;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void ClickOnDismissAlert(int timeout = 2)
        {
            try
            {
                Log.Info("Try to click on alert dismiss button");
                SwitchToAlert(timeout).Dismiss();
                AlertFlag = false;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected bool IsAlertPresent(int timeout = 2)
        {
            try
            {
                Log.Info("Check if alert is present");
                AlertFlag = SwitchToAlert(timeout) == null ? false : true;
                Log.Info($"Alert is present = [{AlertFlag}]");
                return AlertFlag;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        private IAlert SwitchToAlert(int timeout)
        {
            IAlert alert = null;

            try
            {
                alert = _driver.GetWebDriverWait(timeout).Until(d =>
                {
                    try
                    {
                        return _driver.GetDriver().SwitchTo().Alert();
                    }
                    catch (NoAlertPresentException)
                    {
                        return null;
                    }
                });
            }
            catch (WebDriverTimeoutException ex) 
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                alert = null;
            }

            return alert;
        }
    }
}