﻿using OpenQA.Selenium;
using Selenium.WebDriver;

namespace Selenium.SeleniumActions
{
    public abstract class HighlightElementsActions : Reporter
    {
        public HighlightElementsActions(Driver driver) :base(driver) { }

        protected virtual void HighlightElementAndRemoveHighlight(IWebElement element)
        {
            try
            {
                var oldStyle = ElementHighlight(element);
                RemoveHighlight(element, oldStyle);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
            }
        }

        protected virtual string ElementHighlight(IWebElement element, bool isText = false)
        {
            var oldStyle = element.GetAttribute("style");

            if (isText)
                _driver.GetJavaScriptExecutor().ExecuteScript("arguments[0].setAttribute('style', arguments[1]);", element, $"color:blue; border: 3px solid red;");
            else
                _driver.GetJavaScriptExecutor().ExecuteScript("arguments[0].setAttribute('style', arguments[1]);", element, $"{oldStyle} border: 3px solid red;");

            return oldStyle;
        }

        protected virtual void RemoveHighlight(IWebElement element, string style)
        {
            if (!Alerts.AlertFlag)
            {
                try
                {
                    _driver.GetJavaScriptExecutor().ExecuteScript($"arguments[0].setAttribute('style', '{style}')", element);
                }
                catch { }
            }
        }
    }
}