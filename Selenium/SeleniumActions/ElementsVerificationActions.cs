﻿using OpenQA.Selenium;
using Selenium.PageHelpers;
using Selenium.WebDriver;
using SeleniumExtras.WaitHelpers;
using static Logger.Logger;

namespace Selenium.SeleniumActions
{
    public abstract class ElementsVerificationActions : DriverWaitActions
    {
        public ElementsVerificationActions(Driver driver) : base(driver) { }

        protected bool IsElementRowExists(By by, Dictionary<int, string> columnsIndexAndValues)
        {
            try
            {
                Log.Info($"Verify table [{PageBase.GetElementName(by)}], Row: Is Exists");
                Log.Info($"The Expected Row: {GetExpectedRowValues(columnsIndexAndValues)}");
                var table = _driver.GetElement(by);
                var trs = table.FindElements(By.TagName("tr"));
                return true;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        private string GetExpectedRowValues(Dictionary<int, string> columnsIndexAndValues)
        {
            try
            {
                var keys = columnsIndexAndValues.Keys.ToList();
                List<string> values = new List<string>();
                foreach (var key in keys)
                {
                    values.Add(columnsIndexAndValues[key]);
                }

                return string.Join("|", values);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected bool IsElementExists(By by, int timeout = 5)
        {
            try
            {
                Log.Info($"Verify element: [{PageBase.GetElementName(by)}], Is Exists");
                var element = _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementExists(by));
                return element != null;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return false;
            }
        }

        protected bool IsElementEnabled(By by, int timeout = 5)
        {
            try
            {
                Log.Info($"Verify element: [{PageBase.GetElementName(by)}], Is Enabled");
                var element = _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementExists(by));
                var enabled = element.Enabled;
                Log.Info($"{PageBase.GetElementName(by)} Is Enabled = [{enabled}]");
                return enabled;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return false;
            }
        }

        protected bool IsElementDisappeared(By by, int timeout = 5)
        {
            try
            {
                Log.Info($"Wait for: {PageBase.GetElementName(by)} To Disappeard");
                var wait = _driver.GetWebDriverWait(timeout);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                var isDisappeared = wait.Until(ExpectedConditions.InvisibilityOfElementLocated(by));
                Log.Info($"{PageBase.GetElementName(by)} Is Disappeard = [{isDisappeared}]");
                return isDisappeared;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return false;
            }
        }

        protected bool IsElementIsVisible(By by, int timeout = 5)
        {
            try
            {
                Log.Info($"Verify element: [{PageBase.GetElementName(by)}], Is visible");
                _driver.GetWebDriverWait(timeout).Until(ExpectedConditions.ElementIsVisible(by));
            }
            catch (NoSuchElementException ex)
            {
                Log.Info($"Element: [{PageBase.GetElementName(by)}], Is NOT visible");
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return false;
            }

            Log.Info($"Element: [{PageBase.GetElementName(by)}], Is VISIBLE");
            return true;
        }

        protected bool IsElementDisplayed(By by)
        {
            try
            {
                Log.Info($"Check if [{PageBase.GetElementName(by)}], is displayed");
                var displayed = _driver.GetElement(by).Displayed;
                Log.Info($"[{PageBase.GetElementName(by)}], is displayed = [{displayed}]");
                return _driver.GetElement(by).Displayed;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return false;
            }
        }

        protected bool IsElementChecked(By by)
        {
            try
            {
                Log.Info($"Check if element: {PageBase.GetElementName(by)}, Is checked");
                var element = _driver.GetElement(by);
                HighlightElementAndRemoveHighlight(element);
                var selected = element.Selected;
                Log.Info($"Element: {PageBase.GetElementName(by)}, Is Checked = [{selected}]");
                return selected;
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                return false;
            }
        }
    }
}