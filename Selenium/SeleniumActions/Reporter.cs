﻿using Selenium.WebDriver;
using System.Runtime.CompilerServices;
using static Logger.Logger;

namespace Selenium.SeleniumActions
{
    public abstract class Reporter
    {
        protected readonly Driver _driver;

        public Reporter(Driver driver)
        {
            _driver = driver;
        }

        protected void WriteExceptionToLogAndReportAndTakeScreenShot(Exception ex, [CallerMemberName] string methodName = "")
        {
            try
            {
                Log.Error($"[{methodName}] - {ex}");
                var screenShotPath = _driver.TakeScreenShotAndGetThePath();
                Log.Info($"ScreenShot Path: {screenShotPath}");
            }
            catch (Exception exc)
            {
                Log.Error(exc);
            }
        }
    }
}
