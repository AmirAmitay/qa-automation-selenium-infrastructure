﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Interactions;
using Selenium.WebDriver;
using System.Xml.Linq;
using System;
using static OpenQA.Selenium.Interactions.PointerInputDevice;
using OpenQA.Selenium.Appium;


namespace Selenium.SeleniumActions
{
    public class AppiumActionBot : ActionBot
    {
        public AppiumActionBot(Driver driver) : base(driver) { }

        protected override string ElementHighlight(IWebElement element, bool isText = false)
        {
            return string.Empty;
        }

        protected override void HighlightElementAndRemoveHighlight(IWebElement element) { }

        protected override void RemoveHighlight(IWebElement element, string style) { }

        protected void Touch(By by, int yOffset = 0, int xOffset = 0)
        {
            Log.Info($"Try To Tap On Element: [{PageBase.GetElementName(by)}], X Offset: [{xOffset}], Y Offset: [{yOffset}]");

            var element = _driver.GetElement(by);
      Point sourceLocation = element.Location;
      var sourceSize = element.Size;
      int centerX = sourceLocation.X + sourceSize.Width/ 2;
      int centerY = sourceLocation.Y + sourceSize.Height / 2;
      var touch = new PointerInputDevice(PointerKind.Touch, "finger");
      var sequence = new ActionSequence(touch, 1);
      var sequences = new List<ActionSequence>();
      PointerEventProperties pointerEventProperties = new PointerEventProperties();
      pointerEventProperties.Pressure = 1;
      sequence.AddAction(touch.CreatePointerMove(CoordinateOrigin.Viewport, centerX + xOffset, centerY + yOffset, TimeSpan.Zero));
      sequence.AddAction(touch.CreatePointerDown(MouseButton.Touch));
      sequence.AddAction(touch.CreatePointerUp(MouseButton.Touch));
      sequences.Add(sequence);
      ((AppiumDriver)_driver.GetDriver()).PerformActions(sequences);
  }

  protected void Tap(By by, int yOffset = 0, int xOffset = 0)
  {
      Log.Info($"Try To Tap On Element: [{PageBase.GetElementName(by)}], X Offset: [{xOffset}], Y Offset: [{yOffset}]");

      var element = _driver.GetElement(by);
      Point sourceLocation = element.Location;
      var sourceSize = element.Size;
      int centerX = sourceLocation.X + sourceSize.Width/ 2;
      int centerY = sourceLocation.Y + sourceSize.Height / 2;
      var touch = new PointerInputDevice(PointerKind.Touch, "finger");
      var sequence = new ActionSequence(touch, 1);
      var sequences = new List<ActionSequence>();
      PointerEventProperties pointerEventProperties = new PointerEventProperties();
      pointerEventProperties.Pressure = 1;
      sequence.AddAction(touch.CreatePointerMove(CoordinateOrigin.Viewport, centerX + xOffset, centerY + yOffset, TimeSpan.Zero));
      sequence.AddAction(touch.CreatePointerDown(MouseButton.Left));
      sequence.AddAction(touch.CreatePointerUp(MouseButton.Left));
      sequences.Add(sequence);
      ((AppiumDriver)_driver.GetDriver()).PerformActions(sequences);
  }

  protected IWebElement ScrollUnitlFindElement(By by)
  {
      Log.Info($"Try To Scroll To Element Until Is Visible: [{PageBase.GetElementName(by)}]");
      var totalScreenHeight = _driver.GetDriver().Manage().Window.Size.Height;
      int startX = _driver.GetDriver().Manage().Window.Size.Width /2;
      int startY = totalScreenHeight /2;
      int endY = Convert.ToInt32(_driver.GetDriver().Manage().Window.Size.Height * 0.2);
      OpenQA.Selenium.Appium.Interactions.PointerInputDevice touch = new OpenQA.Selenium.Appium.Interactions.PointerInputDevice(PointerKind.Touch, "finger");
      var sequences = new List<ActionSequence>();
      var sequence = new ActionSequence(touch);
      PointerEventProperties pointerEventProperties = new PointerEventProperties();
      pointerEventProperties.Pressure = 1;
      sequence.AddAction(touch.CreatePointerMove(CoordinateOrigin.Viewport, startX, startY, TimeSpan.Zero));
      sequence.AddAction(touch.CreatePointerDown(MouseButton.Touch));
      sequence.AddAction(touch.CreatePointerMove(CoordinateOrigin.Viewport, startX, endY, TimeSpan.FromMilliseconds(600)));
      sequence.AddAction(touch.CreatePointerUp(MouseButton.Touch));
      sequences.Add(sequence);

      var timeout = 2;
      _driver.SetImplicitWait(timeout);
      var pageEnd = 0;
      while (!IsElementExists(by, timeout))
      {
          ((AppiumDriver)_driver.GetDriver()).PerformActions(sequences);
          pageEnd +=endY;
          if (IsElementIsVisible(by, timeout))
          {
              Log.Info("Element Found");
              break;
          }

          if (pageEnd >= totalScreenHeight)
          {
              var ex = new NoSuchElementException("End Of The Page");
              WriteExceptionToLogAndReportAndTakeScreenShot(ex);
              throw ex;
          }
      }

      _driver.SetImplicitWait(_driver.DriverTimeout);

      return _driver.GetElement(by);
  }

  protected IWebElement ScrollUnitlFindElement(By by, int startX)
  {
      Log.Info($"Try To Scroll To Element Until Is Visible: [{PageBase.GetElementName(by)}]");
      var totalScreenHeight = _driver.GetDriver().Manage().Window.Size.Height;
      int startY = totalScreenHeight /2;
      int endY = Convert.ToInt32(_driver.GetDriver().Manage().Window.Size.Height * 0.2);
      OpenQA.Selenium.Appium.Interactions.PointerInputDevice touch = new OpenQA.Selenium.Appium.Interactions.PointerInputDevice(PointerKind.Touch, "finger");
      var sequences = new List<ActionSequence>();
      var sequence = new ActionSequence(touch);
      PointerEventProperties pointerEventProperties = new PointerEventProperties();
      pointerEventProperties.Pressure = 1;
      sequence.AddAction(touch.CreatePointerMove(CoordinateOrigin.Viewport, startX, startY, TimeSpan.Zero));
      sequence.AddAction(touch.CreatePointerDown(MouseButton.Touch));
      sequence.AddAction(touch.CreatePointerMove(CoordinateOrigin.Viewport, startX, endY, TimeSpan.FromMilliseconds(600)));
      sequence.AddAction(touch.CreatePointerUp(MouseButton.Touch));
      sequences.Add(sequence);

      var timeout = 2;
      _driver.SetImplicitWait(timeout);
      var pageEnd = 0;
      while (!IsElementExists(by, timeout))
      {
          ((AppiumDriver)_driver.GetDriver()).PerformActions(sequences);
          pageEnd +=endY;
          if (IsElementIsVisible(by, timeout))
          {
              Log.Info("Element Found");
              break;
          }

          if (pageEnd >= totalScreenHeight)
          {
              var ex = new NoSuchElementException("End Of The Page");
              WriteExceptionToLogAndReportAndTakeScreenShot(ex);
              throw ex;
          }
      }

      _driver.SetImplicitWait(_driver.DriverTimeout);

      return _driver.GetElement(by);
     }
    }
}