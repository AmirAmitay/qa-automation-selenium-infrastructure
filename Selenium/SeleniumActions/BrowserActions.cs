﻿using Selenium.WebDriver;
using System.Diagnostics;
using static Logger.Logger;

namespace Selenium.SeleniumActions
{
    public abstract class BrowserActions : ElementsVerificationActions
    {
        private static int _windowsCount;
        private static string _parentWindow;

        public BrowserActions(Driver driver) : base(driver) { }

        protected void RefreshPage() => _driver.GetDriver().Navigate().Refresh();

        protected void CloseCurrentWindowAndReturnToParentWindow()
        {
            CloseCurrentWindow();
            ReturnToParentWindow();
        }

        internal static void ResetWindowsProperties()
        {
            _windowsCount = 0;
            _parentWindow = string.Empty;
        }

        protected void CloseCurrentWindow()
        {
            try
            {
                Log.Info($"Try To Close Current Window");
                _driver.GetDriver().Close();
                _windowsCount = _driver.GetDriver().WindowHandles.Count;
                Log.Info($"Browser Windows Count = [{_windowsCount}]");
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void ReturnToParentWindow()
        {
            try
            {
                Log.Info($"Try To Return To Parent Window");
                Log.Info($"Browser Windows Count = [{_driver.GetDriver().WindowHandles.Count}]");
                _driver.GetDriver().SwitchTo().Window(_parentWindow);
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected bool SwitchToNewWindow(int timeout = 20)
        {
            try
            {
                return _driver.GetWebDriverWait(timeout).Until(d =>
                {
                    Log.Info($"Old windows count = [{_windowsCount}]");
                    _parentWindow = _driver.GetDriver().CurrentWindowHandle;
                    try
                    {
                        var stopWatch = new Stopwatch();
                        stopWatch.Start();

                        while (_windowsCount == _driver.GetDriver().WindowHandles.Count && stopWatch.Elapsed.TotalSeconds < timeout)
                        {

                        }

                        if (_windowsCount == _driver.GetDriver().WindowHandles.Count)
                        {
                            Log.Info($"Not found new window");
                            return true;
                        }

                        _windowsCount = _driver.GetDriver().WindowHandles.Count;
                        Log.Info($"Windows count = [{_windowsCount}]");
                        var newWindow = _driver.GetDriver().WindowHandles.Last();
                        for (int i = 0; i < _driver.GetDriver().WindowHandles.Count; i++)
                        {
                            Log.Debug($"Window Index = [{i}]");
                            Log.Debug($"Window Handle = {_driver.GetDriver().WindowHandles[i]}");
                        }
                        _driver.GetDriver().SwitchTo().Window(newWindow);
                        _driver.GetDriver().Manage().Window.Maximize();
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        return false;
                    }

                    Log.Debug($"Window Title = [{_driver.GetDriver().Title}]");
                    return true;
                });
            }
            catch (Exception ex)
            {
                WriteExceptionToLogAndReportAndTakeScreenShot(ex);
                throw;
            }
        }

        protected void SwitchToDefaultContet()
        {
            _driver.GetDriver().SwitchTo().DefaultContent();
        }

        protected void ReturnToParentFrame()
        {
            _driver.GetDriver().SwitchTo().ParentFrame();
        }
    }
}
