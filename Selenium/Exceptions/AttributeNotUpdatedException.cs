﻿namespace Selenium.Exceptions
{
    internal class AttributeNotUpdatedException : Exception
    {
        public AttributeNotUpdatedException(string message) : base(message) { }
    }
}