﻿namespace Selenium.Exceptions
{
    internal class TextNotPresentInElementException : Exception
    {
        public TextNotPresentInElementException(string message) : base(message) { }
    }
}