﻿namespace Selenium.Exceptions
{
    internal class TableRowsCountDiffrentFromTheExpectedCount : Exception
    {
        public TableRowsCountDiffrentFromTheExpectedCount(string message) : base(message) { }
    }
}
