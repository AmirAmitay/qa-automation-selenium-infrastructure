﻿namespace Selenium.Exceptions
{
    internal class ElementHaveNoTextException : Exception
    {
        public ElementHaveNoTextException() : base() { }
    }
}