﻿using System.ComponentModel;

namespace Selenium.Enums
{
    public enum HtmlAttributesEnum
    {
        [Description("innerHTML")]
        InnerHTML,
        [Description("textContent")]
        TextContent,
        [Description("value")]
        Value,
        [Description("title")]
        Title,
        [Description("Style")]
        Style,
        [Description("Class")]
        Class,
        [Description("autocomplete")]
        Autocomplete,
        [Description("ng-reflect-value")]
        Ng_Reflect_Value,
        [Description("ng-reflect-model")] 
        Ng_Reflect_Model
    }
}