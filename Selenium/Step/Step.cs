﻿using NUnit.Framework;
using static Logger.Logger;

namespace Selenium.Step
{
    public class Step<T> : Step
    {
        public T ActualResult;

        public Step(Func<T> action, Func<bool> validation) : base()
        {
            InvokeReturnMethods(action);
            InvokeValidationAndAssert(validation);
        }

        public Step(Func<T> action, Func<T, bool> validation)
        {
            InvokeReturnMethods(action);
            InvokeValidationWithParametersAndAssert(validation);
        }

        private void InvokeReturnMethods(Func<T> action)
        {
            ActualResult = action.Invoke();
        }

        private void InvokeValidationWithParametersAndAssert(Func<T, bool> validation)
        {
            try
            {
                if (ActualResult != null)
                {
                    Assert.That(validation.Invoke(ActualResult));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Assert.Fail();
                throw;
            }
        }
    }

    public class Step
    {
        public Step(Action action, Func<bool> validation)
        {
            InvokeVoidMethod(action);
            InvokeValidationAndAssert(validation);
        }

        public Step() { }

        protected void InvokeVoidMethod(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Assert.Fail();
                throw;
            }
        }

        protected void InvokeValidationAndAssert(Func<bool> validation)
        {
            try
            {
                Assert.That(validation.Invoke());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Assert.Fail();
                throw;
            }
        }
    }
}
