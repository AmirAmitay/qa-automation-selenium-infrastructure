﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Diagnostics;
using Selenium.PageHelpers;
using Selenium.SeleniumActions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Appium;
using static Logger.Logger;
using System.Reflection;

namespace Selenium.WebDriver
{
    public class Driver
    {
        private IWebDriver _webDriver;

        public double DriverTimeout { get; set; }

        public Driver(Type driverType, string url = "", double timeout = 10)
        {
            InitDriver(url, driverType, timeout);
        }

        public Driver(Type driverType, DriverOptions options, string url = "", double timeout = 10)
        {
            InitDriver(url, driverType, timeout, options);
        }

        public Driver(Type driverType, Uri uri, AppiumOptions options, double timeout = 10)
        {
            InitMobileDriver(driverType, uri, options, timeout);
        }

        public Driver() { }

        private void InitDriver(string url, Type driverType, double timeout, DriverOptions options = null)
        {
            try
            {
                Log.Info("Create new web driver instance");
                _webDriver = options == null ? CreateNewWebDriverByType(driverType) : CreateNewWebDriverByType(driverType, options);
                Log.Info($"Driver Type: [{driverType}]");

                if (!string.IsNullOrEmpty(url))
                {
                    _webDriver.Url = url;
                    Log.Info($"Driver URL: {url}");
                }

                SetImplicitWait(timeout);
                SetPageLoadTimeout(timeout);
                _webDriver.Manage().Window.Maximize();
                Log.Info("Driver Init Success");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            DriverTimeout = timeout;
        }

        private void InitMobileDriver(Type driverType, Uri uri, AppiumOptions options, double timeout = 10)
        {
            try
            {
                Log.Info("Create new mobile driver instance");
                _webDriver = (IWebDriver)Activator.CreateInstance(driverType, uri, options);
                Log.Info($"Mobile Driver Type: [{driverType}]");
                Log.Info($"Device Name: [{options.DeviceName}]");
                Log.Info($"Driver URI: {uri}");
                SetImplicitWait(timeout);
                Log.Info("Driver Init Success");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }

            DriverTimeout = timeout;
        }

        [Obsolete]
        public void CloseCurrentApplication()
        {
            var driver = ((AppiumDriver)_webDriver);
            driver.CloseApp();
        }

        public void CloseApplication()
        {
            try
            {
                var driver = ((AppiumDriver)_webDriver);
                string appId = driver.Capabilities.GetCapability("appPackage").ToString();
                Log.Info($"Try To Close App: {appId}");
                driver.TerminateApp(appId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public void Quit()
        {
            try
            {
                _webDriver.Quit();
                BrowserActions.ResetWindowsProperties();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public WebDriverWait GetWebDriverWait(int timeout = 10, double interval = 0.5)
        {
            var wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(timeout));
            wait.PollingInterval = TimeSpan.FromSeconds(interval);
            return wait;
        }

        public IWebElement GetElement(By by, bool print = true)
        {
            try
            {
                if (print)
                {
                    Log.Info($"Try to get element: [{PageBase.GetElementName(by)}]");
                }
                var tryCount = 3;

                IWebElement element = null;

                for (int i = 0; i < tryCount; i++)
                {
                    try
                    {
                        element = GetWebDriverWait().Until(e => e.FindElement(by));
                    }
                    catch (StaleElementReferenceException)
                    {
                        element = null;
                        Log.Error($"StaleElementReferenceException: At Try: [{i+1}]");
                    }
                    catch (WebDriverArgumentException)
                    {
                        element = null;
                        Log.Error($"WebDriverArgumentException: At Try: [{i+1}]");
                    }
                    catch (Exception ex)
                    {
                        element = null;
                        Log.Error($"Error: At Try: [{i+1}]");
                        Log.Error(ex);
                    }

                    if (element!=null)
                    {
                        break;
                    }
                }

                return element;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public List<IWebElement> GetElementsList(By by, bool print = true)
        {
            try
            {
                if (print)
                {
                    Log.Info($"Try to get element list of: [{PageBase.GetElementName(by)}]");
                }

                return _webDriver.FindElements(by).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public string TakeScreenShotAndGetThePath()
        {
            if (Alerts.AlertFlag)
            {
                return string.Empty;
            }

            try
            {
                Screenshot screenshot = ((ITakesScreenshot)_webDriver).GetScreenshot();

                string screenshotEncoded = screenshot.AsBase64EncodedString;
                byte[] screenshotAsByteArray = screenshot.AsByteArray;

                string fullPathToReturn = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Screenshots\";

                if (!Directory.Exists(fullPathToReturn))
                {
                    Directory.CreateDirectory(fullPathToReturn);
                }

                fullPathToReturn += DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".png";
                screenshot.SaveAsFile(fullPathToReturn);

                return fullPathToReturn;
            }
            catch
            {
                return string.Empty;
            }
        }

        public void Navigate(string url)
        {
            try
            {
                Log.Info($"Navigate To: {url}");
                _webDriver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public IWebDriver GetDriver()
        {
            return _webDriver;
        }

        public void AddNewTab()
        {
            _webDriver.SwitchTo().NewWindow(WindowType.Tab);
        }

        public IJavaScriptExecutor GetJavaScriptExecutor()
        {
            return ((IJavaScriptExecutor)_webDriver);
        }

        public void SetImplicitWait(double timeout)
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeout);
        }

        public static void KillChromeProcesses()
        {
            Process.GetProcessesByName("chromedriver").ToList().ForEach(p => p.Kill());
            Process.GetProcessesByName("chrome").ToList().ForEach(p => p.Kill());
        }

        private void SetPageLoadTimeout(double timeout)
        {
            Log.Debug($"Set Driver Page Load Timout To: {timeout} Seconds");
            _webDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(timeout);
        }

        private IWebDriver CreateNewWebDriverByType(Type driverType)
        {
            if (driverType == typeof(ChromeDriver))
            {
                //Default Chrome Oprions
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("--ignore-certificate-errors");
                options.AddArguments("--enable-extensions");
                options.AddArguments("--auth-server-whitelist=*");
                return CreateNewWebDriverByType(driverType, options);
            }

            return (IWebDriver)Activator.CreateInstance(driverType);
        }

        private IWebDriver CreateNewWebDriverByType(Type driverType, DriverOptions options)
        {
            return (IWebDriver)Activator.CreateInstance(driverType, options);
        }
    }
}